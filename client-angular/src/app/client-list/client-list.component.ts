import { Component, OnInit } from '@angular/core';
import { ClientService } from '../shared/client/client.service'

import { Client } from '../shared/client/model/client';
@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  clients: Array<Client>;

  constructor(private service: ClientService) { }

  ngOnInit() {
    this.service.getAllClients().subscribe(response => {
      this.clients = response.map(item => {
        return new Client(
          item.id,
          item.uip,
          item.name,
          item.lastName,
          item.dni,
          item.cuil,
          item.dateOfBirthday,
          item.enabled
        );
      });
    })
  }

}
