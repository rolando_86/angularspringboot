export class Client {
    public id: number;
    public uip: string;
    public name: string;
    public lastName: string;
    public dni: number;
    public cuil: string;
    public dateOfBirthday: string;
    public enabled: boolean;

    constructor(id: number, uip: string, name: string, lastName: string, dni: number, cuil: string, dateOfBirthday: string, enabled: boolean) {
        this.id = id;
        this.uip = uip;
        this.name = name;
        this.lastName = lastName;
        this.dni = dni;
        this.cuil = cuil;
        this.dateOfBirthday = dateOfBirthday;
        this.enabled = enabled;
    }
}