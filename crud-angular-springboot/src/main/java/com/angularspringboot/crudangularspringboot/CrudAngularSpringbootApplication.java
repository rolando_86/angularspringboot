package com.angularspringboot.crudangularspringboot;

import com.angularspringboot.crudangularspringboot.entity.Car;
import com.angularspringboot.crudangularspringboot.entity.Client;
import com.angularspringboot.crudangularspringboot.repository.CarRepository;
import com.angularspringboot.crudangularspringboot.repository.ClientRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@SpringBootApplication
public class CrudAngularSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudAngularSpringbootApplication.class, args);
    }

    /*@Bean
    ApplicationRunner init(CarRepository repository) {
        return args -> {
            Stream.of("Ferrari", "Jaguar", "Porsche", "Lamborghini", "Bugatti",
                    "AMC Gremlin", "Triumph Stag", "Ford Pinto", "Yugo GV").forEach(name -> {
                Car car = new Car();
                car.setName(name);
                repository.save(car);
            });
            repository.findAll().forEach(System.out::println);
        };
    }*/

    @Bean
    ApplicationRunner init(ClientRepository repository){
        return args ->{
          SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
          List<Client> listInit = new ArrayList<>();
          Client client1 = createClient("uip498","Rolando","Puma Guzman",32363164,"20-32363164-7",textFormat.parse("1986-06-09") ,true);
          Client client2 = createClient("uip738","Eduardo","Gomez Perez",33823993,"20-33823993-8",textFormat.parse("1990-07-10"),true);
          listInit.add(client1);
          listInit.add(client2);
          listInit.stream().forEach(client -> {
              repository.save(client);
          });
          repository.findAll().forEach(System.out::println);
        };
    }
    private Client createClient(String uip, String name, String lastName, Integer dni, String cuil, Date dateOfBirthday, Boolean enabled) {
        Client client = new Client();
        client.setUip(uip);
        client.setName(name);
        client.setLastName(lastName);
        client.setDni(dni);
        client.setCuil(cuil);
        client.setDateOfBirthday(dateOfBirthday);
        client.setEnabled(enabled);
        return client;
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }
}
