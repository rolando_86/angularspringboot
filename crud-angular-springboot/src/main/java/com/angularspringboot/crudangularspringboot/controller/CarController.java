package com.angularspringboot.crudangularspringboot.controller;

import com.angularspringboot.crudangularspringboot.entity.Car;
import com.angularspringboot.crudangularspringboot.repository.CarRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class CarController {

    private CarRepository carRepository;

    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping("/cars")
    public Collection<Car> getAllCars() {
        return carRepository.findAll().stream().filter(this::bestCar).collect(Collectors.toList());
    }

    private boolean bestCar(Car car) {
        return !car.getName().equals("AMC Gremlin") &&
                !car.getName().equals("Yugo GV") &&
                !car.getName().equals("Triumph Stag");
    }
}
