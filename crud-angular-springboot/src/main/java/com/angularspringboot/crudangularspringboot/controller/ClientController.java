package com.angularspringboot.crudangularspringboot.controller;

import com.angularspringboot.crudangularspringboot.entity.Client;
import com.angularspringboot.crudangularspringboot.repository.ClientRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class ClientController {
    private ClientRepository clientRepository;

    public ClientController(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @GetMapping("/clients")
    public Collection<Client> getAllClients() throws ParseException {
        /*SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");

        Client client1 = createClient("uip498","Rolando","Puma Guzman",32363164,"20-32363164-7",textFormat.parse("1986-06-09") ,true);
        Client client2 = createClient("uip738","Eduardo","Gomez Perez",33823993,"20-33823993-8",textFormat.parse("1990-07-10"),true);
        clientRepository.save(client1);
        clientRepository.save(client2);*/
        return clientRepository.findAll().stream().collect(Collectors.toList());
    }

    private Client createClient(String uip, String name, String lastName, Integer dni,String cuil, Date dateOfBirthday, Boolean enabled) {
        Client client = new Client();
        client.setUip(uip);
        client.setName(name);
        client.setLastName(lastName);
        client.setDni(dni);
        client.setCuil(cuil);
        client.setDateOfBirthday(dateOfBirthday);
        client.setEnabled(enabled);
        return client;
    }
}
