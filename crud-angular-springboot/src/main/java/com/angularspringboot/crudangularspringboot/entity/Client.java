package com.angularspringboot.crudangularspringboot.entity;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private @NonNull String uip;
    private @NonNull String name;
    private @NonNull String lastName;
    private @NonNull Integer dni;
    private @NonNull String cuil;
    private @NonNull Date dateOfBirthday;
    private @NonNull Boolean enabled;
}
