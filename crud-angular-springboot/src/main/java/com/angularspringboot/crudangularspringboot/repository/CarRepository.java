package com.angularspringboot.crudangularspringboot.repository;

import com.angularspringboot.crudangularspringboot.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
public interface CarRepository extends JpaRepository<Car,Long> {
}
